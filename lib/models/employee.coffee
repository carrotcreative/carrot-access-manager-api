db = require '../db'

schema = new db.Schema
  slug: { type: String, default: null }
  name: { type: String, required: true }
  door_code: { type: String, required: true }
  key_fob: { type: String, default: null }
  active: { type: Boolean, default: true }

schema.set 'toJSON',
  transform: (doc, ret, options) ->
    ret.id = ret._id
    delete ret._id
    delete ret.__v
    ret

module.exports = db.model('Employee', schema)

express  = require 'express'
W        = require 'when'
node     = require 'when/node'
Employee = require './models/employee'

app = module.exports = express.Router()

app.get '/employees', (req, res) ->
  get_all_employees()
    .done (models) -> res.json(models)

app.post '/employees', (req, res) ->
  create_employee(req.body)
    .done (model) -> res.json(model)

app.delete '/employees/:id', (req, res) ->
  delete_employee(req.params.id)
    .done -> res.json(success: true)

app.put '/employees/:id', (req, res) ->
  edit_employee(req.params.id, req.body)
    .done (model) -> res.json(model[0])

app.get '/door_code', (req, res) ->
  get_existing_codes()
    .then(generate_unique_code)
    .done (code) -> res.json(code: code)

#
# @api private
#

get_existing_codes = ->
  node.call(Employee.find.bind(Employee))
    .then (all) -> all.map (e) -> e.door_code

generate_unique_code = (codes) ->
  while true
    num = rand(10000, 99999)
    if codes.indexOf(num) < 0 then return num

rand = (min, max) ->
  Math.round(Math.random() * (max - min) + min).toString()

get_all_employees = ->
  node.call(Employee.find.bind(Employee))

create_employee = (data) ->
  W.resolve Employee.create
    name: data.name
    slug: data.slug
    key_fob: data.key_fob
    door_code: data.door_code

edit_employee = (id, data) ->
  node.call(Employee.findOne.bind(Employee), _id: id)
    .then (model) ->
      model.key_fob = data.key_fob or model.key_fob
      model.door_code = data.door_code or model.door_code
      model.active = data.active
      model
    .then (model) -> node.call(model.save.bind(model))

delete_employee = (id) ->
  node.call(Employee.remove.bind(Employee), _id: id)

db  = require './index'

db.connect_db().then ->
  db.connection.db.dropDatabase (err, res) ->
    if err then return console.error(err)
    console.log 'database dropped'
    db.connection.close()

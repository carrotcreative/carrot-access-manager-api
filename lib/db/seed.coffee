db       = require './index'
Employee = require '../models/employee'
W        = require 'when'
node     = require 'when/node'

db.connect_db().then ->
  employee = []

  add employee,
    door_code: '08236'
    name: 'Mike Germano'
    slug: 'mike'

  add employee,
    door_code: '24630'
    name: 'Chris Petescia'
    slug: 'chris'

  add employee,
    door_code: '31467'
    active: false
    name: 'Alain Emile'
    slug: 'alain'
    key_fob: '171740487'

  add employee,
    door_code: '29244'
    active: false
    name: 'Brooke Lonegan'
    slug: 'brooke'
    key_fob: '171740488'

  add employee,
    door_code: '15726'
    name: 'Rianna Mallard'
    slug: 'rianna'

  add employee,
    door_code: '57369'
    active: false
    name: 'Shawn - GreenHouse Cleaning'

  add employee,
    door_code: '64641'
    name: 'Nick Perold'
    slug: 'nick'

  add employee,
    door_code: '28107'
    name: 'Kevin Smith'
    slug: 'kevin'

  add employee,
    door_code: '45497'
    name: 'Adam Katzenback'
    slug: 'adam'

  add employee,
    door_code: '10401'
    name: 'Daniela Asaro'
    slug: 'daniela'

  add employee,
    door_code: '55365'
    name: 'Kyle MacDonald'
    slug: 'kyle'
    key_fob: '171625203'

  add employee,
    door_code: '37976'
    name: 'Tom Milewski'
    slug: 'tom'

  add employee,
    door_code: '35743'
    name: 'Tony Briceno'
    slug: 'tony'

  add employee,
    door_code: '96256'
    name: 'Jeff Escalante'
    slug: 'jeff'

  add employee,
    door_code: '49643'
    name: 'Steve Badowski'
    slug: 'steve'

  add employee,
    door_code: '58705'
    active: false
    name: 'Marisa Plescia'
    slug: 'marisa'

  add employee,
    door_code: '98421'
    name: 'Brian Christu'
    slug: 'brian'

  add employee,
    door_code: '75619'
    active: false
    name: 'Tim Katt'
    key_fob: '171740489'

  add employee,
    door_code: '31625'
    active: false
    name: 'Alexis Lamster'
    slug: 'alexis'
    key_fob: '171875810'

  add employee,
    door_code: '79438'
    name: 'Juliette Richey'
    slug: 'juliette'

  add employee,
    door_code: '10752'
    active: false
    name: 'Steven Neamonitakis'
    slug: 'stevenneamonitakis'
    key_fob: '171370100'

  add employee,
    door_code: '12902'
    active: false
    name: 'Bob Whitney'
    slug: 'bob'

  add employee,
    door_code: '66808'
    active: false
    name: 'Sunny Eckerle'
    slug: 'sunny'

  add employee,
    door_code: '24296'
    active: false
    name: 'Darryl Ohrt'
    slug: 'darryl'

  add employee,
    door_code: '49115'
    active: false
    name: 'Bonnie Harriman'
    slug: 'bonnie'

  add employee,
    door_code: '35962'
    name: 'Kathryn Farwell'
    slug: 'kathryn'
    key_fob: '171860905'

  add employee,
    door_code: '78241'
    name: 'Sean Aaron'
    slug: 'sean'

  add employee,
    door_code: '38475'
    active: false
    name: 'Unknown'

  add employee,
    door_code: '63852'
    active: false
    name: 'Lee Ourand'
    slug: 'lee'

  add employee,
    door_code: '64188'
    name: 'Matt Indellicati'
    slug: 'mattindellicati'

  add employee,
    door_code: '84858'
    active: false
    name: 'Haley Moore'

  add employee,
    door_code: '23629'
    active: false
    name: 'Amy Benziger'
    slug: 'amybenziger'
    key_fob: '171740512'

  add employee,
    door_code: '43847'
    name: 'Ryan Mack'
    slug: 'ryan'
    key_fob: '171740305'

  add employee,
    door_code: '77236'
    active: false
    name: 'Joshua Wetstone'
    slug: 'joshwetstone'

  add employee,
    door_code: '23103'
    name: 'Kevin Pruett'
    slug: 'kevinpruett'
    key_fob: '171625133'

  add employee,
    door_code: '78200'
    name: 'Brittany Wallace'
    slug: 'brittany'

  add employee,
    door_code: '27147'
    active: false
    name: 'Winter'
    key_fob: '171740511'

  add employee,
    door_code: '81358'
    active: false
    name: 'Jack Chu'

  add employee,
    door_code: '52588'
    active: false
    name: 'Paul Fredrich'

  add employee,
    door_code: '97090'
    name: 'Bridget Brennan'
    slug: 'bridget'
    key_fob: '1370019'

  add employee,
    door_code: '46332'
    active: false
    name: 'Everald Philip'
    slug: 'everald'

  add employee,
    door_code: '36535'
    name: 'Unassigned'

  add employee,
    door_code: '24220'
    name: 'Amanda Rue'
    slug: 'amanda'
    key_fob: '171624857'

  add employee,
    door_code: '78712'
    name: 'Kaitlin Vignali'
    slug: 'kaitlinvignali'
    key_fob: '117625132'

  add employee,
    door_code: '29281'
    active: false
    name: 'Claudia Cukrov'
    slug: 'claudia'
    key_fob: '171624890'

  add employee,
    door_code: '33951'
    name: 'Emily Okey'
    slug: 'emily'
    key_fob: '171624934'

  add employee,
    door_code: '20204'
    active: false
    name: 'Justin Ave'

  add employee,
    door_code: '12769'
    name: 'Bruno Germansderfer'
    slug: 'bruno'
    key_fob: '171625172'

  add employee,
    door_code: '32698'
    active: false
    name: 'Sara Intrator'
    key_fob: '171625069'

  add employee,
    door_code: '89211'
    active: false
    name: 'Rikin Diwan'
    slug: 'rikin'
    key_fob: '171625013'

  add employee,
    door_code: '59758'
    name: 'Lloyd Johnson'
    slug: 'lloyd'
    key_fob: '17187528'

  add employee,
    door_code: '88716'
    active: false
    name: 'R.G. Logan'
    slug: 'rg'
    key_fob: '171625210'

  add employee,
    door_code: '68893'
    name: 'Jason Judy'

  add employee,
    door_code: '32214'
    name: 'Gary Shyenkman'
    slug: 'gary'
    key_fob: '171740667'

  add employee,
    door_code: '36276'
    name: 'Gabrielle Schaefer'
    slug: 'gabrielle'
    key_fob: '171625206'

  add employee,
    door_code: '14906'
    name: 'Noah Atkinson'
    slug: 'noah'
    key_fob: '171625131'

  add employee,
    door_code: '98106'
    active: false
    name: 'Ryan Spence'
    slug: 'ryanspence'
    key_fob: '171740633'

  add employee,
    door_code: '27928'
    name: 'Mitch Kapler'
    slug: 'mitch'
    key_fob: '171875905'

  add employee,
    door_code: '36935'
    name: 'Lenny Neslin'
    slug: 'lenny'
    key_fob: '171876010'

  add employee,
    door_code: '64706'
    name: 'Alec Coughlin'
    slug: 'alec'

  add employee,
    door_code: '69382'
    active: false
    name: 'Adam Jonas/Joe Burdick'
    slug: 'adamjonas'
    key_fob: '171740348'

  add employee,
    door_code: '19865'
    name: 'Averie Timm'
    slug: 'averie'
    key_fob: '171740347'

  add employee,
    door_code: '36008'
    name: 'Ben Cochrane'
    slug: 'ben'
    key_fob: '171740360'

  add employee,
    door_code: '48488'
    name: 'Andrew Tully'
    slug: 'andrew'
    key_fob: '171740440'

  add employee,
    door_code: '13747'
    name: 'Don Povia'
    slug: 'don'
    key_fob: '171740261'

  add employee,
    door_code: '26557'
    active: false
    name: 'Jesse Jordan'
    slug: 'jesse'
    key_fob: '171740319'

  add employee,
    door_code: '46321'
    active: false
    name: 'Kim David'
    slug: 'kim'
    key_fob: '171740240'

  add employee,
    door_code: '41654'
    active: false
    name: 'Ben Sorensen'
    slug: 'bensorensen'
    key_fob: '171875948'

  add employee,
    door_code: '83729'
    name: 'JMS Cleaning'
    key_fob: '171740520'

  add employee,
    door_code: '56207'
    name: 'Alex Rainone'
    slug: 'alex'
    key_fob: '171740527'

  add employee,
    door_code: '23942'
    name: 'Jonathan Santoro'
    slug: 'jonathan'
    key_fob: '171861079'

  add employee,
    door_code: '66549'
    active: false
    name: 'Ryan Locks'
    slug: 'ryanlocks'
    key_fob: '171477007'

  add employee,
    door_code: '89236'
    active: false
    name: 'Farouk Ajakaiye'
    slug: 'farouk'
    key_fob: '171875809'

  add employee,
    door_code: '30199'
    active: false
    name: 'Tim Sweeney'
    slug: 'timsweeney'
    key_fob: '171860826'

  add employee,
    door_code: '47442'
    name: 'Andrew Glassett'
    slug: 'andrewglassett'
    key_fob: '171860826'

  add employee,
    door_code: '34398'
    name: 'Alain Emile'
    slug: 'alain'
    key_fob: '171740487'

  add employee,
    door_code: '01408'
    active: false
    name: 'Brooke Lonegan'
    slug: 'brooke'
    key_fob: '171740488'

  add employee,
    door_code: '22221'
    active: false
    name: 'Tim Katt'
    slug: 'tim'
    key_fob: '171740489'

  add employee,
    door_code: '34999'
    name: 'Josh Rowley'
    slug: 'josh'
    key_fob: '171875948'

  add employee,
    door_code: '02332'
    name: 'Marion Brewer'
    slug: 'marion'
    key_fob: '171740511'

  add employee,
    door_code: '83220'
    name: 'Kurt Reckziegel'
    slug: 'kurt'
    key_fob: '171740240'

  add employee,
    door_code: '26534'
    name: 'Sarah McCormick'
    slug: 'sarah'
    key_fob: '171625211'

  add employee,
    door_code: '20088'
    name: 'Harris Cullinan'
    slug: 'harris'
    key_fob: '171861190'

  add employee,
    door_code: '90140'
    name: 'Henry Dickson'
    slug: 'henry'
    key_fob: '171875991'

  add employee,
    door_code: '58497'
    name: 'Graeme Metcalf'
    slug: 'graeme'
    key_fob: '171876000'

  add employee,
    door_code: '66257'
    name: 'Emily Chambliss'
    slug: 'emilychambliss'
    key_fob: '17187568'

  add employee,
    door_code: '52377'
    name: 'Ivey Inman'
    slug: 'ivey'
    key_fob: '171875599'

  add employee,
    door_code: '43632'
    active: false
    name: 'Henry Snopek'
    slug: 'henrysnopek'
    key_fob: '171875916'

  add employee,
    door_code: '44941'
    name: 'Noah Portes Chaikin'
    slug: 'noahporteschaikin'
    key_fob: '171875626'

  add employee,
    door_code: '56599'
    name: 'Caleb Kramer'
    slug: 'caleb'
    key_fob: '171875861'

  add employee,
    door_code: '17339'
    active: false
    name: 'Yunzhe Zhou'
    slug: 'yunzhe'
    key_fob: '171875917'

  add employee,
    door_code: '32361'
    name: 'Cristina Rodriguez'
    key_fob: '171875577'

  add employee,
    door_code: '63630'
    name: 'Miki Masuda'
    slug: 'miki'
    key_fob: '171875862'

  add employee,
    door_code: '16203'
    name: 'Brandon Romano'
    slug: 'brandon'
    key_fob: '171875648'

  add employee,
    door_code: '52226'
    name: 'Sean Lang'
    slug: 'seanlang'
    key_fob: '171875915'

  add employee,
    door_code: '18564'
    active: false
    name: 'Meghan Fredrich'
    slug: 'meghan'
    key_fob: '171875752'

  add employee,
    door_code: '37373'
    name: 'Daneka Kulikowski'
    slug: 'daneka'
    key_fob: '171861092'

  add employee,
    door_code: '40161'
    name: 'Mike Keisman'
    slug: 'mikekeisman'
    key_fob: '171875887'

  add employee,
    door_code: '20661'
    name: 'Madison Olson'
    slug: 'madison'
    key_fob: '171860907'

  add employee,
    door_code: '32844'
    active: false
    name: 'Alexa Martinez'
    slug: 'alexa'
    key_fob: '171875918'

  add employee,
    door_code: '37980'
    name: 'Kate Black'
    slug: 'kate'
    key_fob: '171860906'

  add employee,
    door_code: '08663'
    name: 'Whitney Brown'
    slug: 'whitney'
    key_fob: '171961930'

  add employee,
    door_code: '32774'
    name: 'Evan Brock'
    slug: 'evan'
    key_fob: '171875914'

  add employee,
    door_code: '99690'
    name: 'Gitamba Saila-Ngita'
    slug: 'gitamba'
    key_fob: '171787078'

  add employee,
    door_code: '00269'
    name: 'Kim David'
    slug: 'kim'
    key_fob: '171961881'

  add employee,
    door_code: '78464'
    name: 'Jesse Arno'
    slug: 'jessearno'
    key_fob: '171961931'

  add employee,
    door_code: '10786'
    name: 'Matt Bond'
    slug: 'mattbond'
    key_fob: '171892442'

  add employee,
    door_code: '76653'
    name: 'Lauren Fodero'
    slug: 'lauren'
    key_fob: '171740345'

  add employee,
    door_code: '04466'
    active: false
    name: 'Rabi Abonour'
    slug: 'rabi'
    key_fob: '171935758'

  add employee,
    door_code: '22190'
    name: 'Marissa Squeri'
    slug: 'marissa'
    key_fob: '171819381'

  add employee,
    door_code: '77459'
    name: 'Peter Whalen'
    slug: 'peter'
    key_fob: '171961946'

  add employee,
    door_code: '08232'
    name: 'Nina Choi'
    slug: 'nina'
    key_fob: '171801679'

  add employee,
    door_code: '40920'
    name: 'John Duncan'
    slug: 'johnduncan'
    key_fob: '171875817'

  add employee,
    door_code: '49751'
    name: 'Andrew Weiss'
    slug: 'andrewweiss'
    key_fob: '171811000'

  add employee,
    door_code: '01984'
    name: 'Ben Gage'
    slug: 'bengage'
    key_fob: '171825152'

  add employee,
    door_code: '86697'
    name: 'Emma Kieckhafer'
    slug: 'emma'
    key_fob: '171819407'

  add employee,
    door_code: '17682'
    name: 'Harun Zankel'
    slug: 'harun'
    key_fob: '171892547'

  add employee,
    door_code: '09524'
    name: 'Perry Ironhill'
    slug: 'perry'
    key_fob: '171892536'

  add employee,
    door_code: '69347'
    name: 'Christine Melgarejo'
    slug: 'christine'
    key_fob: '171729268'

  add employee,
    door_code: '32610'
    name: 'Mike Faley'
    slug: 'mikefaley'
    key_fob: '171935758'

  add employee,
    door_code: '26621'
    name: 'Jaime Cheng'
    slug: 'jaime'
    key_fob: '171893601'

  add employee,
    door_code: '35599'
    name: 'Asif Kahn'
    slug: 'asif'
    key_fob: '171893598'

  add employee,
    door_code: '59358'
    name: 'Antonio Jakes'
    slug: 'antonio'
    key_fob: '171893599'

  add employee,
    door_code: '29858'
    name: 'Priyanka Pulijal'
    slug: 'priyanka'
    key_fob: '171893597'

  add employee,
    door_code: '69085'
    name: 'Hart Hagerty'
    slug: 'hart'
    key_fob: '171943507'

  add employee,
    door_code: '38186'
    name: 'Varta Hojjat'
    slug: 'varta'
    key_fob: '171943598'

  add employee,
    door_code: '29037'
    name: 'Anthony Ciolino'
    slug: 'anthony'
    key_fob: '171943610'

  add employee,
    door_code: '68720'
    name: 'Amanda Yang'
    slug: 'amandayang'
    key_fob: '171943611'

  add employee,
    door_code: '67363'
    name: 'Rachel London'
    slug: 'rachel'
    key_fob: '171943612'

  add employee,
    door_code: '34977'
    name: 'Stef Kruzick'
    slug: 'stef'
    key_fob: '171943613'

  add employee,
    door_code: '89109'
    name: 'Christopher Makris'

  add employee,
    door_code: '37001'
    name: 'David Walker'
    key_fob: '171892394'

  add employee,
    door_code: '38748'
    name: 'Claire Wind'
    slug: 'claire'
    key_fob: '171943608'

  add employee,
    door_code: '42577'
    name: 'Bob Dominguez'
    slug: 'bob'
    key_fob: '171943604'

  add employee,
    door_code: '60197'
    name: 'Elaine Park Andrade'
    slug: 'elaine'
    key_fob: '171892406'

  save_all(employee)
    .then -> console.log 'done!'
    .catch(console.error)
    .then -> db.connection.close()

#
# @api private
#

add = (e, data) ->
  e.push(new Employee(data))

save_all = (e) ->
  W.map e, (e) -> node.call(e.save.bind(e))

mongoose = require 'mongoose'
W        = require 'when'

module.exports = mongoose

module.exports.connect_db = ->
  d = W.defer()

  if process.env.NODE_ENV == 'production'
    mongoose.connect('mongodb://carrot:3074whitney@ds051970.mongolab.com:51970/carrot-access-manager')
  else if process.env.NODE_ENV == 'test'
    mongoose.connect('mongodb://carrot:3074whitney@ds053090.mongolab.com:53090/carrot-access-manager-dev')
  else
    mongoose.connect('localhost', 'carrot-door-codes')

  mongoose.connection.once('open', d.resolve.bind(d))
  mongoose.connection.once('error', d.reject.bind(d))

  return d.promise

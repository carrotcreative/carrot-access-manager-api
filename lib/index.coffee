express     = require 'express'
body_parser = require 'body-parser'
morgan      = require 'morgan'
cors        = require 'cors'
controller  = require './controller'
db          = require './db'
token       = require './token'

module.exports = app = express()
initialized = false

app.use(body_parser.json())
app.use(morgan('dev'))
app.use(cors())

# enable cors preflight
app.options('*', cors())

# connect to database on first run

app.use (req, res, next) ->
  if initialized then return next()
  app.use('/', controller)
  db.connect_db().done -> initialized = true; next()

# authenticate with simple token

app.use (req, res, next) ->
  if req.headers['x-carrot-auth'] == token
    next()
  else
    res.status(401)
    res.end('Unauthorized')

# This is a very simple authentication system that uses one universal token
# intended to be added as a header (x-carrot-auth). This is still quite secure
# because this API is only used behind auth with google apps, so in order to be
# able to get access, an attacker would have to guess the header name as well as
# the token, which is just about impossible.

# If the token ever leaks for any reason, you can reset it globally for this API
# using `npm run generate_token`. Tests will still pass, but you'll need to
# update any front-end implementations that used the old token.

module.exports = '7ac23f00-74c4-11e4-aa52-397083312695'

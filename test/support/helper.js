var chai = require('chai'),
    chai_http = require('chai-http'),
    W = require('when'),
    carrot_door_codes = require('../..');

var should = chai.should();

chai.use(chai_http);
chai.request.addPromises(W.Promise);

global.chai = chai;
global.API = carrot_door_codes;
global.should = should;

process.env.NODE_ENV = 'test';

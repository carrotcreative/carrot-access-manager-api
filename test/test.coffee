token = require '../lib/token'
test_id = null

describe 'API', ->

  it 'should return 401 if not using the correct token', (done) ->
    chai.request(API).get('/door_code').then (res) ->
      res.status.should.equal(401)
      done()

  it 'GET /door_code should return a unique door code', (done) ->
    chai.request(API).get('/door_code').set('X-Carrot-Auth', token).then (res) ->
      if res.error then return done(res.error)
      JSON.parse(res.text).code.should.be.a('string')
      done()

  it 'GET /employees should return all employees', (done) ->
    chai.request(API).get('/employees').set('X-Carrot-Auth', token)
      .then (res) ->
        res = JSON.parse(res.text)
        res.should.be.an('array')
        res[0].should.be.an('object')
        done()

  it 'POST /employees should create a new employee entry', (done) ->
    chai.request(API).get('/door_code').set('X-Carrot-Auth', token)
      .then (res) ->
        if res.error then throw res.error
        JSON.parse(res.text).code
      .then (code) ->
        chai.request(API).post('/employees').set('X-Carrot-Auth', token).send
          name: 'Testy McTesterson'
          slug: 'test'
          door_code: code
          key_fob: '123456'
      .then (res) ->
        res = JSON.parse(res.text)
        res.name.should.equal('Testy McTesterson')
        res.slug.should.equal('test')
        res.key_fob.should.equal('123456')
        res.id.should.be.a.string
        res.door_code.should.be.a.number
        res.active.should.be.true
        test_id = res.id
        done()
      .catch (err) -> done(err)

  it 'PUT /employees/:id should update an existing employee entry', (done) ->
    chai.request(API).put("/employees/#{test_id}").set('X-Carrot-Auth', token).send
      key_fob: '999999'
      door_code: '12345'
    .then (res) ->
      if res.error then return done(res.error)
      res = JSON.parse(res.text)
      res.name.should.equal('Testy McTesterson')
      res.slug.should.equal('test')
      res.key_fob.should.equal('999999')
      res.door_code.should.equal('12345')
      res.active.should.be.true
      done()

  it 'DELETE /employees/:id should remove an employee entry', (done) ->
    chai.request(API).delete("/employees/#{test_id}").set('X-Carrot-Auth', token)
      .then (res) ->
        if res.error then return done(res.error)
        JSON.parse(res.text).success.should.be.true
        done()

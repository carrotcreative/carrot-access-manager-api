uuid = require 'node-uuid'
fs   = require 'fs'
path = require 'path'

token_path = path.resolve('lib/token.coffee')
fs.writeFileSync token_path, "module.exports = '#{uuid.v1()}'"

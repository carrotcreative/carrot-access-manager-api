# Carrot Access Manager

A small API to store and manage door codes and key fob numbers for carrot employees.

> **Note:** This project is in early development, and versioning is a little different. [Read this](http://markup.im/#q4_cRZ1Q) for more details.

### Getting Started

```sh
$ git clone https://github.com/carrot/carrot-access-manager.git
$ cd carrot-access-manager
$ npm i
$ npm start
```

### Database

This project uses mongo as a database since it's a simple, flat data structure. You can seed the database with all employees that were on record before this app was deployed, ported over from the spreadsheet in which we used to manage employee door codes using the command `npm run db:seed`, and you can drop the database and clear everything with `npm run db:reset` if you need to locally.

When the app is running in production, it connects to a small [MongoLab](https://mongolab.com/) instance, the configuration for this is in `db/index.coffee`. Mongolab was chosen because of it's stability, daily backups, and the fact that it's free for such a small and limited use as this app.

### Authentication

This API uses a very basic token-based authentication. This type of auth is only secure if the front-end is behind a truly authenticated wall. For example, the primary implementation of this app will be behind google apps auth, so an attacker would have to guess the token or be authenticted through carrot google apps and view the source in order to gain access. That means that this API should not be utilized anywhere that the front end is open to the public, as anyone would be able to simply view source to gain full access to the API, as you need to send the token with any request, which means it would appear in the source.

The token can be found in `/lib/token.coffee`, and if it is compromised for any reason, can be regenerated using the command `npm run generate_token`. This command will remove the old token and replace it with a new, so any sites that use it will be broken and get `401`s back from any request until they are updated with the new token.

In order to use the token, it should be set as the value of a `X-Carrot-Auth` header in each request.

### Endpoints

##### `GET /door_code`

Returns a unique 5-digit door code for an employee. Example response:

```js
{ code: '12345' }
```

##### `GET /employees`

Returns a list of all employee models with their door codes etc. Returns a JSON array.

##### `POST /employees`

Creates a new employee with associated access code information. Expected parameters (formatted as JSON):

```coffee
name: 'String, required - Employee full name'
slug: 'String, optional - Employee slug from API'
door_code: 'String, required - 5 digit unique door code'
key_fob: 'String, optional - Number associated with building key fob'
```

Returns a JSON representation of the created model.

##### `PUT /employees/:id`

Edits the employee's door code or key fob number. Expected parameters (formatted as JSON):

```coffee
door_code: 'String, optional - 5 digit unique door code'
key_fob: 'String, optional - Number associated with building key fob'
active: 'Boolean, optional - Whether the door code and key fob are activated'
```

Returns a JSON representation of the updated model.

##### `DELETE /employees/:id`

Removes an employee from the system. Not recommended for public implementation. Returns `{ success: true}`

### Staging and Production

This API is running in production at https://carrot-access-manager.herokuapp.com -- ask in the dev channel to be added for deploy access.

### Contributing

- Details on running tests and contributing [can be found here](contributing.md)
